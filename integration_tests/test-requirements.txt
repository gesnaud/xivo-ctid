docker-compose==1.4.2
nose
python-consul
psycopg2==2.5.4
pyhamcrest
sqlalchemy==0.9.8

# xivo-ctid
-e ..

# dependencies for xivo-ctid db code
git+https://gitlab.com/xivo.solutions/xivo-dao.git@xivo-16.08
git+https://gitlab.com/xivo.solutions/xivo-lib-python.git@xivo-16.08

xivo-test-helpers>=0.1.4
